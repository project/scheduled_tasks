<?php

/**
 * @file
 * Code for the Scheduled Tasks.
 */

/**
 * Administration settings form.
 */
function scheduled_tasks_config($form, &$form_state) {
  $form['scheduled_tasks_batch_items'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('scheduled_tasks_batch_items', 20),
    '#required' => TRUE,
    '#title' => t('Batch items per cron run'),
  );

  return system_settings_form($form);
}
