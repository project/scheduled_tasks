SUMMARY
----------------
Scheduled Task module helps developers to schedule executing any function 
using cron.


REQUIREMENTS
----------------
Elysia Cron
https://www.drupal.org/project/elysia_cron


INSTALLATION
----------------
Install this module as usual
http://drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
----------------
1. Enable module Schedule Task.
2. Go to admin/config/services/task and set how many tasks should be 
   processed in one batch (cron run)
3. Save configuration.

USAGE EXAMPLES
----------------

You want to schedule (for example on every Sunday at 9:00 AM) send mail 
notifications to all customers, that made order this week. All you need 
to do is call following function rigth after order is done:

scheduled_tasks_add($start_time, $function_name, $argument);

$start_time - unix timestamp of our schedule time (of current week)
$function_name - name of function to call in scheduled time (function to 
                 send mail notificatios)
$arguments - arguments for $function_name in array
